	##
	#	Manager Device Display
	#	Copyright (C) 2015  KERBOUL 'kiloWatt' William
	#
	#	This program is free software: you can redistribute it and/or modify
	#	it under the terms of the GNU General Public License as published by
	#	the Free Software Foundation, either version 3 of the License, or
	#	(at your option) any later version.
	#
	#	This program is distributed in the hope that it will be useful,
	#	but WITHOUT ANY WARRANTY; without even the implied warranty of
	#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	#	GNU General Public License for more details.
	#
	#	You should have received a copy of the GNU General Public License
	#	along with this program.  If not, see <http://www.gnu.org/licenses/>.


FMC = {

	_serviceableNode: 'instrumentation/cdu/serviceable',

	Behavior: {
		_default: func {
			if (getprop (FMC._serviceableNode) != '1') {
				me.backgroundColor = 'rgb(0, 0, 0)';
				me.display = false;
			}

			# Ajout d'une pagination si il y a plus d'une page.
			if (me.nbPage > 1) append (me.page.field, {'name': 'pagination', 'value': me.pageNum ~ '/' ~ me.nbPage});

			# Ajout du bloc note (sratchpad)
			append (me.page.field, {'name': 'scratchpad', 'value': ''});

			return me.useDefautlBehavior (me); # Utilisation du Behavior par defaut de la classe DEVICE
		},

		'fmc/route': func {
			me = me.myDefaultBehavior (me);

			var depaAirportField = MDD.Helper.findByName ('left-1', me.page.field);
			var depaRunwayField = MDD.Helper.findByName ('left-2', me.page.field);
			var destAirportField = MDD.Helper.findByName ('right-1', me.page.field);

			var data = getprop (depaAirportField.data);
			if (data != nil) {
				depaAirport = airportinfo (data);
				if (depaAirport == nil and data != depaAirportField.defaultValue) {
					depaAirportField.value = depaAirportField.defaultValue;
					MDD.Helper.findByName ('scratchpad', me.page.field).value = 'NOT IN DATABASE';
					props.getNode (io.dirname (depaAirportField.data)).removeChildren (io.basename (depaAirportField.data));
					return me;
				}
			}

			var data = getprop (destAirportField.data);
			if (data != nil) {
				destAirport = airportinfo (data);
				if (destAirport == nil and data != destAirportField.defaultValue) {
					destAirportField.value = destAirportField.defaultValue;
					MDD.Helper.findByName ('scratchpad', me.page.field).value = 'NOT IN DATABASE';
					props.getNode (io.dirname (destAirportField.data)).removeChildren (io.basename (destAirportField.data));
					return me;
				}
			}

			var data = getprop (depaRunwayField.data);
			if (data != nil and depaAirport != nil) {
				if (!contains (depaAirport.runways, data)) {
					depaRunwayField.value = depaRunwayField.defaultValue;
					MDD.Helper.findByName ('scratchpad', me.page.field).value = 'NOT IN DATABASE';
					props.getNode (io.dirname (depaAirportField.data)).removeChildren (io.basename (depaRunwayField.data));
					return me;
				}
			}

			return me;
		},

		'fmc/menu': func {
			return me.myDefaultBehavior (me);
		},

		'ground-service/ext': func {
			me = me.myDefaultBehavior (me);

			var primary = MDD.Helper.findByName ('left-1', me.page.field);
			var secondary = MDD.Helper.findByName ('left-2', me.page.field);

			if (getprop (primary.data) == '1') {
				primary.color ='rgb (0, 255, 0)';
				primary.value = 'CONNECTED';
			} else {
				primary.color = 'rgb (255, 0, 0)';
				primary.value = 'DISCONNECTED';
			}

			if (getprop (secondary.data) == '1') {
				secondary.color ='rgb (0, 255, 0)';
				secondary.value = 'CONNECTED';
			} else {
				secondary.color = 'rgb (255, 0, 0)';
				secondary.value = 'DISCONNECTED';
			}

			return me;
		}
	},

	Buttons: {
		LSK: func (LSK) {
			var this = globals [me.name];

			# Vérifie que la touche LSK ai une action, et que la fonction de cette action existe dans le Device principale
			if (contains (me.actions, LSK) and contains (this.Functions, me.actions [LSK].action.function))
				call (this.Functions [me.actions [LSK].action.function], [me.actions [LSK], me], this);
		},

		prevPage: func {
			if ((me.pageNum-1) >= 1) 
				call (globals [me.name].render, [MDD.Helper.parseLink (me.viewName, (me.pageNum-1))], globals [me.name]);
		},

		nextPage: func {
			if ((me.pageNum+1) <= me.nbPage)
				call (globals [me.name].render, [MDD.Helper.parseLink (me.viewName, (me.pageNum+1))], globals [me.name]);
		},

		AlphaNum: func (char) {
			var ret = MDD.Helper.findByName ('scratchpad', me.page.field);

			if (ret.value == 'DELETE') me.page.field [ret.index].value = '';
			me.page.field [ret.index].value = me.page.field [ret.index].value ~ char;

			globals [me.name].update (me);
		},

		Clear: func () {
			var ret = MDD.Helper.findByName ('scratchpad', me.page.field);

			if (ret.value == 'DELETE') me.page.field [ret.index].value = '';
				
			me.page.field [ret.index].value = left (me.page.field [ret.index].value, size (me.page.field [ret.index].value)-1);

			globals [me.name].update (me);
		},

		Cleard: func {},

		Delete: func {
			var ret = MDD.Helper.findByName ('scratchpad', me.page.field);

			me.page.field [ret.index].value = 'DELETE';

			globals [me.name].update (me);
		},

		Exec: func {
			if (!contains (me, 'callback')) return false;
			var callback = me.callback;
			delete (me, 'callback');
			call (callback, [], me);
		},
	},

	Functions: {		
		callback: func (data) {
			if (contains (globals [me.name].Functions, data.action.param))
				globals [me.name].press.callback = globals [me.name].Functions [data.action.param];
		},

		flightplan: func {
			var dep = getprop ('instrumentation/fmc/data/route/departure/airport');
			var rwy = getprop ('instrumentation/fmc/data/route/departure/runway');
			var des = getprop ('instrumentation/fmc/data/route/destination/airport');
			if (dep != rwy != des != nil) {
				debug.dump (dep);
				setprop ('autopilot/route-manager/departure/airport', dep);
				setprop ('autopilot/route-manager/departure/runway', rwy);
				setprop ('autopilot/route-manager/destination/airport', des);
				setprop ('autopilot/route-manager/active', 'true');

				return print ('FlightPlan');
			}
		},

		'connect-power': func (field, content) {
			var node = field.data;
			var state = getprop (node);

			if (state == '1') {
				setprop (node, 'false');
			} else {
				setprop (node, 'true');
			}
			me.update (content);
		}
	},
};

var devicesName = ["FMCcpt", "FMCfo", "FMCobs"];

if (!contains (MDD, '_currentVersion')) {
	var MDD = MDD.MDD;
}

var settings = MDD.Helper.getFile ("/Models/Instruments/FMC/settings/mdd-settings.nas");


var FMC_INIT = func {
	foreach (var deviceName; devicesName) {
		globals [deviceName] = MDD.Device.new (FMC, settings, deviceName);
		globals [deviceName].render (settings ['boot-page']);
	}
};

	FMC_INIT ();

setlistener(FMC._serviceableNode, FMC_INIT, 0);